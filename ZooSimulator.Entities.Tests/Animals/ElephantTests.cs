﻿
using NUnit.Framework;
using ZooSimulator.Entities.Enums;

// Alias used due to conflicting namespaces.
using SimulatedElephant = ZooSimulator.Entities.Animals.Elephant;

namespace ZooSimulator.Entities.Tests.Animals
{
    /// <summary>
    /// Tests for the <see cref="SimulatedElephant"/> class.
    /// </summary>
    public class ElephantTests
    {
        /// <summary>
        /// Ensure that the maximum health of the animal is correct when it is initialized.
        /// </summary>
        [Test]
        public void Elephant_UponConstruction_SetsMaximumHealth()
        {
            const float expectedHealth = 100;
            var elephant = new SimulatedElephant();

            Assert.AreEqual(expectedHealth, elephant.Health.HealthValue);
        }

        /// <summary>
        /// Ensure that the <see cref="ZooAnimal.AnimalType"/> property is set correctly.
        /// </summary>
        [Test]
        public void Elephant_UponConstruction_SetsAnimalType()
        {
            const ZooAnimalType expectedType = ZooAnimalType.Elephant;

            var elephant = new SimulatedElephant();

            Assert.AreEqual(expectedType, elephant.AnimalType);
        }
    }
}
