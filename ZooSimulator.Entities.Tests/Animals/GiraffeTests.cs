﻿using NUnit.Framework;
using ZooSimulator.Entities.Enums;
using SimulatedGiraffe = ZooSimulator.Entities.Animals.Giraffe;

namespace ZooSimulator.Entities.Tests.Animals
{
    /// <summary>
    /// Tests for the <see cref="SimulatedGiraffe"/> class.
    /// </summary>
    public class GiraffeTests
    {
        /// <summary>
        /// Ensure that the maximum health of the animal is correct when it is initialized.
        /// </summary>
        [Test]
        public void Elephant_UponConstruction_SetsMaximumHealth()
        {
            const float expectedHealth = 100;
            var giraffee = new SimulatedGiraffe();

            Assert.AreEqual(expectedHealth, giraffee.Health.HealthValue);
        }

        /// <summary>
        /// Ensure that the <see cref="ZooAnimal.AnimalType"/> property is set correctly.
        /// </summary>
        [Test]
        public void Giraffe_UponConstruction_SetsAnimalType()
        {
            const ZooAnimalType expectedType = ZooAnimalType.Giraffe;

            var giraffe = new SimulatedGiraffe();

            Assert.AreEqual(expectedType, giraffe.AnimalType);
        }
    }
}
