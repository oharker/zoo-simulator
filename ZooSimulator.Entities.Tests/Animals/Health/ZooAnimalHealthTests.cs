﻿using Moq;
using NUnit.Framework;
using ZooSimulator.Entities.Animals.Health;

namespace ZooSimulator.Entities.Tests.Animals.Health
{
    /// <summary>
    /// Tests for the <see cref="ZooAnimalHealth"/>
    /// </summary>
    public class ZooAnimalHealthTests
    {
        /// <summary>
        /// Ensure that the maximum health value is set correctly.
        /// </summary>
        [Test]
        public void ZooAnimalHealth_GivenConstructor_HasMaximumHealth()
        {
            float expectedHealth = 100;
            var mockHealth = new Mock<ZooAnimalHealth>();

            Assert.AreEqual(expectedHealth, mockHealth.Object.HealthValue);
        }
         
        /// <summary>
        /// Ensure that health is calculated correctly when the <see cref="ZooAnimalHealth.Decrease(float)"/>
        /// method is called.
        /// </summary>
        /// <param name="decreasePercentage">The decrease percentage used.</param>
        /// <returns>The heatlh value.</returns>
        [TestCase(50f, ExpectedResult = 50f)]
        [TestCase(22.45f, ExpectedResult = 77.55f)]
        [TestCase(22.434224f, ExpectedResult = 77.5657806f)]
        [TestCase(1.5f, ExpectedResult = 98.5f)]
        [TestCase(1000f, ExpectedResult = 0f)]
        public float ZooAnimalHealth_GivenDecrease_DeteriatesPercentage(float decreasePercentage)
        {
            var mockHealth = new Mock<ZooAnimalHealth>();

            mockHealth.Object.Decrease(decreasePercentage);

            return mockHealth.Object.HealthValue;
        }

        /// <summary>
        /// Ensure that health is calculated correctly when the <see cref="ZooAnimalHealth.Increase(float)"/>
        /// method is called.
        /// </summary>
        /// <param name="increasePercentage">The decrease percentage used.</param>
        /// <returns>The health value.</returns>
        [TestCase(50f, ExpectedResult = 75f)]
        [TestCase(22.45f, ExpectedResult = 61.225f)]
        [TestCase(22.434224f, ExpectedResult = 61.2171097f)]
        [TestCase(1.5f, ExpectedResult = 50.75f)]
        [TestCase(1000f, ExpectedResult = 100f)]
        public float ZooAnimalHealth_GivenIncrease_IncreasesPercentage(float increasePercentage)
        {
            var mockHealth = new Mock<ZooAnimalHealth>();

            // Set all health to 50%.
            mockHealth.Object.Decrease(50);

            mockHealth.Object.Increase(increasePercentage);

            return mockHealth.Object.HealthValue;
        }
    }
}
