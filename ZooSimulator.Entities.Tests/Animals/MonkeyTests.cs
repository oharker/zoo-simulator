﻿using NUnit.Framework;
using ZooSimulator.Entities.Enums;

// Alias used due to confused namespaces.
using SimulatedMonkey = ZooSimulator.Entities.Animals.Monkey;

namespace ZooSimulator.Entities.Tests.Animals
{
    /// <summary>
    /// Tests for the <see cref="SimulatedMonkey"/> class.
    /// </summary>
    public class MonkeyTests
    {
        /// <summary>
        /// Ensure that the maximum health of the animal is correct when it is initialized.
        /// </summary>
        [Test]
        public void Monkey_UponConstruction_SetsMaximumHealth()
        {
            const float expectedHealth = 100;
            var monkey = new SimulatedMonkey();

            Assert.AreEqual(expectedHealth, monkey.Health.HealthValue);
        }

        /// <summary>
        /// Ensure that the <see cref="ZooAnimal.AnimalType"/> property is set correctly.
        /// </summary>
        [Test]
        public void Monkey_UponConstruction_SetsAnimalType()
        {
            const ZooAnimalType expectedType = ZooAnimalType.Monkey;

            var monkey = new SimulatedMonkey();

            Assert.AreEqual(expectedType, monkey.AnimalType);
        }
    }
}
