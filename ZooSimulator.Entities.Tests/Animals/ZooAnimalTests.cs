using Moq;
using NUnit.Framework;
using System;
using ZooSimulator.Entities.Animals;
using ZooSimulator.Entities.Enums;
using ZooSimulator.Entities.Exception;

namespace Tests.Animals
{
    /// <summary>
    /// Tests for the <see cref="ZooAnimal"/> base class.
    /// </summary>
    public class ZooAnimalTests
    {
        /// <summary>
        /// Ensure that if a null argument is provided to the <see cref="ZooAnimal.Eat(ZooSimulator.Entities.Food.IFood)" method,
        /// a <see cref="ArgumentNullException"/> is thrown.
        /// </summary>
        [Test]
        public void Eat_GivenNullFood_ThrowsArgumentNullException()
        {
            try
            {
                // Setup the mock animal.
                var mockAnimal = new Mock<ZooAnimal>(ZooAnimalType.Monkey);

                // Provide null to the mock animal.
                mockAnimal.Object.Eat(null);
            }
            catch (ArgumentNullException ex)
            {
                // Ensure the correct exception is thrown.
                Assert.AreEqual("food", ex.ParamName);
                Assert.AreEqual("Food cannot be null or empty.\r\nParameter name: food", ex.Message);
            }
        }

        /// <summary>
        /// Ensure that marking the animal as decased sets the status correctly.
        /// </summary>
        [Test]
        public void MarkAsDeceased_SetsStatus()
        {
            var mockAnimal = new Mock<ZooAnimal>(ZooAnimalType.Monkey);

            mockAnimal.Object.MarkAsDeceased();

            Assert.AreEqual(ZooAnimalStatus.Deceased, mockAnimal.Object.Status);
        }

        /// <summary>
        /// Ensure that an exception is thrown when an animal is attempted to be deteriated when 
        /// it is dead.
        /// </summary>
        [Test]
        public void DeteriateHealth_GivenAnimalDeceased_ThrowsException()
        {
            var mockAnimal = new Mock<ZooAnimal>(ZooAnimalType.Monkey);

            mockAnimal.Object.MarkAsDeceased();

            try
            {
                mockAnimal.Object.DeteriorateHealth(50);
                Assert.Fail($"{nameof(InvalidAnimalActionException)} not thrown.");
            }
            catch(InvalidAnimalActionException exception)
            {
                Assert.AreEqual("Animal's health cannot deteriate - it is deceased.", exception.Message);
            }
        }
    }
}