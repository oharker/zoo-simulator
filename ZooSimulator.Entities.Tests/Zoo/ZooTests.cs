﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using ZooSimulator.Entities.Animals.Interfaces;
using SimulatedZoo = ZooSimulator.Entities.Zoo.Zoo;

namespace ZooSimulator.Entities.Tests.Zoo
{
    /// <summary>
    /// Tests for the <see cref="Zoo"/> entity.
    /// </summary>
    public class ZooTests
    {
        /// <summary>
        /// Ensure that a <see cref="ArgumentNullException"/> is thrown when null is provided.
        /// </summary>
        [Test]
        public void Zoo_GivenNull_ThrowsArgumentNullException()
        {
            try
            {
                var zoo = new SimulatedZoo(null);
                Assert.Fail($"{nameof(ArgumentNullException)} not thrown.");
            }
            catch (ArgumentNullException exception)
            {
                Assert.AreEqual(exception.Message, "Value cannot be null.\r\nParameter name: Animals cannot be null.");
            }
        }

        /// <summary>
        /// Ensure that the provided <see cref="IEnumerable{IZooAnimal}"/> to the constructor
        /// are set within the <see cref="SimulatedZoo.Animals"/> property.
        /// </summary>
        [Test]
        public void Zoo_GivenAnimals_SetsAnimals()
        {
            var animals = new List<IZooAnimal>()
            {
                new Mock<IZooAnimal>().Object,
                new Mock<IZooAnimal>().Object,
                new Mock<IZooAnimal>().Object,
                new Mock<IZooAnimal>().Object,
            };

            var zoo = new SimulatedZoo(animals);
            Assert.AreEqual(animals, zoo.Animals);
        }
    }
}
