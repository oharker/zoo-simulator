﻿using ZooSimulator.Entities.Animals.Health;
using ZooSimulator.Entities.Enums;

namespace ZooSimulator.Entities.Animals
{
    /// <summary>
    /// Represents an elephant zoo animal.
    /// </summary>
    public class Elephant : ZooAnimal
    {
          
        /// <summary>
        /// Initializes a new instance of the elephant animal.
        /// </summary>
        public Elephant() : base(ZooAnimalType.Elephant)
        {
            Health.OnHealthChanged += Health_OnHealthChanged;
        }

        /// <summary>
        /// The health for the elephant class.
        /// </summary>
        public override ZooAnimalHealth Health { get; protected set; } = new ElephantHealth();

        /// <summary>
        /// Method called when health is changed for the animal.
        /// </summary>
        private void Health_OnHealthChanged()
        {
            if (Health.HealthValue < ElephantHealth.WalkingImpairedThreshold)
            {
                // We are afflicted if we reach below this point.
                Status = ZooAnimalStatus.WalkingImpaired;
            }
            else
            {
                Status = ZooAnimalStatus.Alive;
            }
        }
    }
}
