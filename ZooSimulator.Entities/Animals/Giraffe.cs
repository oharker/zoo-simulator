﻿using ZooSimulator.Entities.Animals.Health;
using ZooSimulator.Entities.Enums;

namespace ZooSimulator.Entities.Animals
{
    /// <summary>
    /// Represents a giraffe zoo animal.
    /// </summary>
    public class Giraffe : ZooAnimal
    {
        /// <summary>
        /// The health for the giraffee class.
        /// </summary>
        public override ZooAnimalHealth Health { get; protected set; } = new GiraffeAnimalHealth();

        /// <summary>
        /// Initializes a new instance of the giraffe animal.
        /// </summary>
        public Giraffe() : base(ZooAnimalType.Giraffe)
        {
        }
    }
}
