﻿
namespace ZooSimulator.Entities.Animals.Health
{
    /// <summary>
    /// Represents health for an <see cref="Elephant"/> within the zoo.
    /// </summary>
    public class ElephantHealth : ZooAnimalHealth
    {
        /// <summary>
        /// Initializes a new instance of the Elephant Health class.
        /// </summary>
        public ElephantHealth()
        {
            OnHealthChanged += Health_OnHealthChanged;
        }

        /// <summary>
        /// The walking impaired threshold for health.
        /// </summary>
        public const float WalkingImpairedThreshold = 70;

        /// <summary>
        /// Boolean value as to whether the health is required to improve before the next
        /// check in order to satisfy the minimum requirements
        /// for the animal.
        /// </summary>
        private bool requiresHealthImprovement = false;

        /// <summary>
        /// Returns a boolean value as to whether the health is below minimum requirements for the animal.
        /// </summary>
        /// <returns>A Boolean value as to whether the health is below minimum requirements for the animal.</returns>
        public override bool IsBelowMinimumRequirements()
        {
            bool isBelow = false;
            if (HealthValue < WalkingImpairedThreshold)
            {
                if (requiresHealthImprovement == false)
                {
                    // Health has not yet been checked. Ensure this is checked.
                    requiresHealthImprovement = true;
                }
                else
                {
                    isBelow = true;
                }
            }

            return isBelow;
        }

        /// <summary>
        /// Method called when health is changed for the animal.
        /// </summary>
        private void Health_OnHealthChanged()
        {
            // Reset requires health improvement flag.
            if (requiresHealthImprovement && HealthValue >= WalkingImpairedThreshold)
            {
                requiresHealthImprovement = false;
            }
        }
    }
}
