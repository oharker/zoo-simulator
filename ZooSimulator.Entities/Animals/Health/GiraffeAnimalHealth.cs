﻿namespace ZooSimulator.Entities.Animals.Health
{
    /// <summary>
    /// Represents health for an <see cref="Giraffe"/> within the zoo.
    /// </summary>
    public class GiraffeAnimalHealth : ZooAnimalHealth
    {
        /// <summary>
        /// The Health threshold for the for health.
        /// </summary>
        private const float healthThreshold = 50;

        /// <summary>
        /// Returns a boolean value as to whether the health is below minimum requirements for the animal.
        /// </summary>
        /// <returns>A Boolean value as to whether the health is below minimum requirements for the animal.</returns>
        public override bool IsBelowMinimumRequirements()
        {
            return HealthValue < healthThreshold;
        }
    }
}
