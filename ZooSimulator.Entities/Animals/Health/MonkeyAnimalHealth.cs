﻿namespace ZooSimulator.Entities.Animals.Health
{
    public class MonkeyAnimalHealth : ZooAnimalHealth
    {
        /// <summary>
        /// The Health threshold for the for health.
        /// </summary>
        private const float healthThreshold = 30;

        /// <summary>
        /// Returns a boolean value as to whether the health is below minimum requirements for the animal.
        /// </summary>
        /// <returns>A Boolean value as to whether the health is below minimum requirements for the animal.</returns>
        public override bool IsBelowMinimumRequirements()
        {
            return HealthValue < healthThreshold;
        }
    }
}
