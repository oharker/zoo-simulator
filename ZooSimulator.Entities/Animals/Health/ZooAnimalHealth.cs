﻿using System;
using System.Collections.Generic;
using ZooSimulator.Entities.Animals.Interfaces;

namespace ZooSimulator.Entities.Animals.Health
{
    /// <summary>
    /// Represents the health of a <see cref="IZooAnimal"/>.
    /// </summary>
    public abstract class ZooAnimalHealth
    {
        /// <summary>
        /// The maximum health value for the zoo animal.
        /// </summary>
        private const float _maximumHealth = 100;

        /// <summary>
        /// The minimum health value for the zoo animal.
        /// </summary>
        private const float _minimumHealth = 0;

        /// <summary>
        /// The current value of the animal's health.
        /// </summary>
        public float HealthValue { get; private set; } = _maximumHealth;

        /// <summary>
        /// Event raised when the <see cref="HealthValue"/> has been changed.
        /// </summary>
        public event Action OnHealthChanged;

        /// <summary>
        /// Increase the health value.
        /// </summary>
        /// <param name="increasePercentage">The percentage value to increase health by.</param>
        /// <remarks>Is limited by the maximum health value.</remarks>
        public void Increase(float increasePercentage)
        {
            // Ensure that the health value cannot exeed the value depicted by the maximum.
            var newHealthValue = HealthValue + (HealthValue * increasePercentage / 100);
            if (newHealthValue > _maximumHealth)
            {
                HealthValue = _maximumHealth;
            }
            else
            {
                HealthValue = newHealthValue;
            }

            OnHealthChanged?.Invoke();
        }

        /// <summary>
        /// Decrease the health value.
        /// </summary>.
        /// <param name="deteriationPercentage">The percentage to decrease the health value by.</param>
        public void Decrease(float deteriationPercentage)
        {
            var newHealthValue = HealthValue - (HealthValue * deteriationPercentage / 100);
            if (newHealthValue < 0)
            {
                HealthValue = _minimumHealth;
            }
            else
            {
                HealthValue = newHealthValue;
            }

            OnHealthChanged?.Invoke();
        }

        /// <summary>
        /// Returns a boolean value as to whether the animal's health is below the minimum requirements.
        /// </summary>
        /// <returns>Boolean value as to whether the animal's health is below the minimum requirements.</returns>
        public abstract bool IsBelowMinimumRequirements();
    }
}
