﻿using ZooSimulator.Entities.Animals.Health;
using ZooSimulator.Entities.Enums;
using ZooSimulator.Entities.Food;

namespace ZooSimulator.Entities.Animals.Interfaces
{
    /// <summary>
    /// Interface for an animmal within the zoo.
    /// </summary>
    public interface IZooAnimal
    {
        /// <summary>
        /// The current status of the zoo animal.
        /// </summary>
        ZooAnimalStatus Status { get; }

        /// <summary>
        /// The type of animal.
        /// </summary>
        ZooAnimalType AnimalType { get; }

        /// <summary>
        /// Eat the food provided.
        /// </summary>
        /// <param name="food">The food to eat.</param>
        void Eat(IFood food);

        /// <summary>
        /// The current health value of the zoo animal.
        /// </summary>
        ZooAnimalHealth Health { get; }

        /// <summary>
        /// Deteriate the health of the zoo animal.
        /// </summary>
        /// <param name="deteriationPercentage">The percentage to deteriate the health by.</param>
        void DeteriorateHealth(float deteriationPercentage);

        /// <summary>
        /// Declare the animal as deceased.
        /// </summary>
        void MarkAsDeceased();
    }
}
