﻿using ZooSimulator.Entities.Animals.Health;
using ZooSimulator.Entities.Enums;

namespace ZooSimulator.Entities.Animals
{
    /// <summary>
    /// Represents a monkey zoo animal.
    /// </summary>
    public class Monkey : ZooAnimal
    {
        /// <summary>
        /// Initializes a new instance of the Monkey animal.
        /// </summary>
        public Monkey() : base(ZooAnimalType.Monkey)
        {
        }

        /// <summary>
        /// The health for the monkey class.
        /// </summary>
        public override ZooAnimalHealth Health { get; protected set; } = new MonkeyAnimalHealth();
    }
}
