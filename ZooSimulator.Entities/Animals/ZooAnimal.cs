﻿using System;
using ZooSimulator.Entities.Animals.Health;
using ZooSimulator.Entities.Animals.Interfaces;
using ZooSimulator.Entities.Enums;
using ZooSimulator.Entities.Exception;
using ZooSimulator.Entities.Food;

namespace ZooSimulator.Entities.Animals
{
    /// <summary>
    /// Abstract class representing a zoo animal.
    /// </summary>
    public abstract class ZooAnimal : IZooAnimal
    {
        /// <summary>
        /// Gets the current status of the zoo animal.
        /// </summary>
        public ZooAnimalStatus Status { get; protected set; }

        /// <summary>
        /// Gets the type of animal.
        /// </summary>
        public ZooAnimalType AnimalType { get; private set; }

        /// <summary>
        /// Gets the health of the zoo animal.
        /// </summary>
        public virtual ZooAnimalHealth Health { get; protected set; }

        /// <summary>
        /// Create a new instance of the <see cref="ZooAnimal"/>.
        /// </summary>
        public ZooAnimal(ZooAnimalType animalType)
        {
            AnimalType = animalType;
        }

        /// <summary>
        /// Eat the food provided.
        /// </summary>
        /// <param name="food">The food to eat.</param>
        /// <exception cref="InvalidAnimalActionException">
        /// Thrown when the animal's <see cref="Status"/> is set to <see cref="ZooAnimalStatus.Deceased"/>.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// Thrown when the provided <see cref="IFood"/> is null.
        /// </exception>
        public void Eat(IFood food)
        {
            if (food == null)
            {
                throw new ArgumentNullException(nameof(food), "Food cannot be null or empty.");
            }

            if (Status == ZooAnimalStatus.Deceased)
            {
                throw new InvalidAnimalActionException("Animal cannot eat - it is deceased.");
            }

            Health.Increase(food.Value);
        }

        /// <summary>
        /// Deteriate the health of the zoo animal.
        /// </summary>
        /// <param name="deteriationPercentage">The percentage to deteriate the health by.</param>
        /// <exception cref="InvalidAnimalActionException">
        /// Thrown when the animal's <see cref="Status"/> is set to <see cref="ZooAnimalStatus.Deceased"/>.
        /// </exception>
        public void DeteriorateHealth(float deteriationPercentage)
        {
            if (Status == ZooAnimalStatus.Deceased)
            {
                throw new InvalidAnimalActionException("Animal's health cannot deteriate - it is deceased.");
            }

            Health.Decrease(deteriationPercentage);
        }

        /// <summary>
        /// Declare the animal deceased.
        /// </summary>
        public void MarkAsDeceased()
        {
            Status = ZooAnimalStatus.Deceased;
        }
    }
}
