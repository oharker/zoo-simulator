﻿namespace ZooSimulator.Entities.Enums
{
    /// <summary>
    /// Represents the status of a <see cref="ZooAnimal"/>
    /// </summary>
    public enum ZooAnimalStatus
    {
        /// <summary>
        /// Represents that the animal is alive.
        /// </summary>
        Alive = 0,

        /// <summary>
        /// Represents that the animal is injured.
        /// </summary>
        WalkingImpaired = 1,

        /// <summary>
        /// Represents that the animal is deceased.
        /// </summary>
        Deceased = 2
    }
}
