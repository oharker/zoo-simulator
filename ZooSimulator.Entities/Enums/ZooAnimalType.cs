﻿namespace ZooSimulator.Entities.Enums
{
    /// <summary>
    /// Represents a type of animal within the zoo.
    /// </summary>
    public enum ZooAnimalType
    {
        /// <summary>
        /// Represents a giraffe.
        /// </summary>
        Giraffe = 0,

        /// <summary>
        /// Represents a monkey.
        /// </summary>
        Monkey = 1,

        /// <summary>
        /// Represents an elephant.
        /// </summary>
        Elephant = 2
    }
}
