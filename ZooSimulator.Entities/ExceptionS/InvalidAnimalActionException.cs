﻿using System;

namespace ZooSimulator.Entities.Exception
{
    /// <summary>
    /// Exception for invalid actions performed on animals.
    /// </summary>
    [Serializable]
    public class InvalidAnimalActionException : System.Exception
    {
        public InvalidAnimalActionException() { }
        public InvalidAnimalActionException(string message) : base(message) { }
        public InvalidAnimalActionException(string message, System.Exception inner) : base(message, inner) { }
        protected InvalidAnimalActionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
