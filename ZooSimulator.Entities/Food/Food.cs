﻿using System;

namespace ZooSimulator.Entities.Food
{
    /// <summary>
    /// Represents food within the zoo simulator.
    /// </summary>
    public class Food : IFood
    {
        /// <summary>
        /// Gets or sets the value of the food.
        /// </summary>
        public float Value { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Food"/> class.
        /// </summary>
        public Food()
        {
            var random = new Random();
            Value = random.Next(10, 20);
        }
    }
}
