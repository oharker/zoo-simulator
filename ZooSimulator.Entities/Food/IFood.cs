﻿
namespace ZooSimulator.Entities.Food
{
    /// <summary>
    /// Represents food used within the zoo.
    /// </summary>
    public interface IFood
    {
        /// <summary>
        /// The nutritional value of the food.
        /// </summary>
        float Value { get;}
    }
}
