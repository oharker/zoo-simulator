﻿using System;
using System.Collections.Generic;
using ZooSimulator.Entities.Animals;
using ZooSimulator.Entities.Animals.Interfaces;

namespace ZooSimulator.Entities.Zoo
{
    /// <summary>
    /// A Zoo, containing animals.
    /// </summary>
    public class Zoo
    {
        /// <summary>
        /// The animals within the zoo.
        /// </summary>
        public IEnumerable<IZooAnimal> Animals { get; private set; }
        
        /// <summary>
        /// Initializes a new zoo class, with the animals provided.
        /// </summary>
        /// <param name="animals">The animals to construct the <see cref="IZoo"/> with.</param>
        public Zoo(IEnumerable<IZooAnimal> animals)
        {
            Animals = animals ?? throw new ArgumentNullException("Animals cannot be null.");
        }
    }
}
