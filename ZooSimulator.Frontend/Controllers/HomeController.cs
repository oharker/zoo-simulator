﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ZooSimulator.Frontend.Models;
using ZooSimulator.Services.ZooServices.Interfaces.Time.Interfaces;
using ZooSimulator.Services.ZooServices.Zoo.Interfaces;

namespace ZooSimulator.Frontend.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// The zoo service.
        /// </summary>
        IZooService _zooService;

        /// <summary>
        /// The time service.
        /// </summary>
        IZooTimeService _timeService;

        /// <summary>
        /// Initializes a new instance of the home controller.
        /// </summary>
        /// <param name="zooService">The zoo service.</param>
        /// <param name="zooTimeService">The zoo time service.</param>
        public HomeController(IZooService zooService, IZooTimeService zooTimeService)
        {
            _zooService = zooService;
            _timeService = zooTimeService;
        }

        /// <summary>
        /// The index for the home controller.
        /// </summary>
        /// <returns>Result of the operation.</returns>
        [HttpGet]
        public IActionResult Index()
        {
            var zoo = _zooService.GetZoo();

            var viewModel = new ZooStatusViewModel(zoo.Animals, _timeService.GetTime());

            return View(viewModel);
        }

        /// <summary>
        /// Retrieve the animals for the zoo.
        /// </summary>
        /// <returns>Result of the operation.</returns>
        [HttpGet]
        public JsonResult GetAnimals()
        {
            var zoo = _zooService.GetZoo();
            var viewModels = zoo.Animals.Select(m => new ZooAnimalStatusViewModel(m));

            return Json(new { Data = viewModels });
        }

        /// <summary>
        /// Retrieves the summary for the zoo.
        /// </summary>
        /// <returns>Result of the operation.</returns>
        [HttpGet]
        public JsonResult GetSummary()
        {
            var zoo = _zooService.GetZoo();

            var viewModel = new ZooStatusViewModel(zoo.Animals, _timeService.GetTime());

            return Json(viewModel);
        }

        /// <summary>
        /// Feed the zoo.
        /// </summary>
        /// <returns>Result of the operation.</returns>
        [HttpPost]
        public IActionResult Feed()
        {
            _zooService.FeedAnimals();

            return Ok();
        }
    }
}
