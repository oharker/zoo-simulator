﻿using ZooSimulator.Entities.Enums;

namespace ZooSimulator.Frontend.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="ZooAnimalStatus"/> enum.
    /// </summary>
    public static class ZooAnimalStatusExtensions
    {
        /// <summary>
        /// Converts the provided <see cref="ZooAnimalStatus"/> enum to its string equivalent.
        /// </summary>
        /// <param name="status">The status of the zoo animal.</param>
        /// <returns>A string representing the status of the <see cref="ZooAnimal"/>.</returns>
        public static string ToViewModelString(this ZooAnimalStatus status)
        {
            var returnValue = "Unknown";
            switch (status)
            {
                case ZooAnimalStatus.Alive:
                    returnValue = "Alive";
                    break;
                case ZooAnimalStatus.WalkingImpaired:
                    returnValue = "Cannot Walk";
                    break;
                case ZooAnimalStatus.Deceased:
                    returnValue = "Deceased";
                    break;
            }

            return returnValue;
        }
    }
}
