using System;

namespace ZooSimulator.Frontend.Models
{
    /// <summary>
    /// View model for displaying any errors.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// The ID of the request, if any.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// A boolean value as to whether the request Id should be shown.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}