﻿using ZooSimulator.Entities.Animals.Interfaces;
using ZooSimulator.Frontend.Extensions;

namespace ZooSimulator.Frontend.Models
{
    /// <summary>
    /// Represents the status of a single <see cref="IZooAnimal"/> entity.
    /// </summary>
    public class ZooAnimalStatusViewModel
    {
        /// <summary>
        /// Represents the health value for the view model.
        /// </summary>
        public string Health;

        /// <summary>
        /// Represents the status value for the view model.
        /// </summary>
        public string Status;

        /// <summary>
        /// Represents the type of animal for the view model.
        /// </summary>
        public string Type;

        /// <summary>
        /// Intializes a new instance of the <see cref="ZooAnimalStatusViewModel"/> class.
        /// </summary>
        /// <param name="zooAnimal">The zoo animal to create the view model from.</param>
        public ZooAnimalStatusViewModel(IZooAnimal zooAnimal)
        {
            Type = zooAnimal.AnimalType.ToString();
            Status = zooAnimal.Status.ToViewModelString();
            Health = (zooAnimal.Health.HealthValue / 100).ToString("P2");
        }
    }
}
