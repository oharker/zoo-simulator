﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZooSimulator.Entities.Animals.Interfaces;

namespace ZooSimulator.Frontend.Models
{
    /// <summary>
    /// View model containing the status of the whole zoo.
    /// </summary>
    public class ZooStatusViewModel
    {
        /// <summary>
        /// Gets the total number of animals.
        /// </summary>
        public int TotalAnimalsCount { get; private set; }

        /// <summary>
        /// Gets the total number of alive animals.
        /// </summary>
        public int AliveAnimalsCount { get; private set; }

        /// <summary>
        /// Gets the deceased animal count.
        /// </summary>
        public int DeceasedAnimalsCount { get; private set; }

        /// <summary>
        /// Gets the afflicted animal count.
        /// </summary>
        public int AfflictedAnimalsCount { get; private set; }

        /// <summary>
        /// Gets the child view models of <see cref="IZooAnimal"/> entities.
        /// </summary>
        public IEnumerable<ZooAnimalStatusViewModel> AnimalViewModels { get; private set; }

        /// <summary>
        /// Gets the current time.
        /// </summary>
        public DateTime Time { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZooStatusViewModel"/>.
        /// </summary>
        /// <param name="zooAnimals">The zoo animals to create the view model with.</param>
        /// <param name="currentTime">The current time.</param>
        public ZooStatusViewModel(IEnumerable<IZooAnimal> zooAnimals, DateTime currentTime)
        {
            TotalAnimalsCount = zooAnimals.Count();
            AliveAnimalsCount = zooAnimals.Count(animal => animal.Status != Entities.Enums.ZooAnimalStatus.Deceased);
            DeceasedAnimalsCount = zooAnimals.Count(animal => animal.Status == Entities.Enums.ZooAnimalStatus.Deceased);
            AnimalViewModels = zooAnimals.Select(animal => new ZooAnimalStatusViewModel(animal));
            Time = currentTime;
        }
    }
}
