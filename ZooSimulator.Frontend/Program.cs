﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ZooSimulator.Frontend
{
    /// <summary>
    /// Main entry point of the application.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main entry point of the web application.
        /// </summary>
        /// <param name="args">Arguments to the application.</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
