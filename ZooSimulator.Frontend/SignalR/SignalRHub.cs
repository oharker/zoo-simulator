﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace ZooSimulator.Frontend
{
    public class SignalRHub : Hub
    {
        /// <summary>
        /// Notify listening SignalR clients that the time has changed.
        /// </summary>
        /// <param name="time">The time that has been changed to.</param>
        /// <returns>Task to be awaited.</returns>
        public async Task NotifyTimeChanged(DateTime time)
        {
            await Clients.All.SendAsync("TimeChanged", time.ToString("h:mm tt"));
        }

        /// <summary>
        /// Notify listening SignalR clients that the zoo state has changed.
        /// </summary>
        /// <returns>Task to be awaited.</returns>
        public async Task NotifyZooStateChanged()
        {
            await Clients.All.SendAsync("ZooChanged");
        }
    }
}
