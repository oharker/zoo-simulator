﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ZooSimulator.Entities.Zoo;
using ZooSimulator.Services.Configuration;
using ZooSimulator.Services.ZooServices;
using ZooSimulator.Services.ZooServices.Interfaces.Time.Interfaces;
using ZooSimulator.Services.ZooServices.Time;
using ZooSimulator.Services.ZooServices.Zoo.Interfaces;

namespace ZooSimulator.Frontend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add services
            services.AddSingleton<Zoo, Zoo>();
            services.AddSingleton<SignalRHub, SignalRHub>();
            services.AddSingleton<IZooService, ZooService>();
            services.AddSingleton<IZooTimeService, ZooTimeService>();

            // Add MVC.
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add our Config objects so they can be injected
            services.Configure<ZooTimeServiceConfiguration>(Configuration.GetSection("ZooTimeConfiguration"));
            services.Configure<ZooConfiguration>(Configuration.GetSection("ZooConfiguration"));

            // Add Signal R
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider provider)
        {
            var zooService = provider.GetService<IZooService>();
            var timeService = provider.GetService<IZooTimeService>();
            var signalRHub = provider.GetService<SignalRHub>();

            // Register event handlers
            timeService.OnHourChange += () => zooService.DeteriateAnimals();
            timeService.OnTimeChange += (time) => signalRHub.NotifyTimeChanged(time);
            zooService.OnZooStateChange += () => signalRHub.NotifyZooStateChanged();

            // Start the time service.
            timeService.Start();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Setup signalR
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalRHub>("/timehub");
            });

            app.UseMvc();
        }
    }
}
