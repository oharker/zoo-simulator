﻿using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using ZooSimulator.Entities.Animals;
using ZooSimulator.Services.Configuration;

namespace ZooSimulator.Services.Tests.Builders
{
    /// <summary>
    /// Tests for the <see cref="AnimalsBuilder"/> class.
    /// </summary>
    public class AnimalsBuilderTests
    {
        /// <summary>
        /// Ensure that not calling the <see cref="{nameof(AnimalsBuilder.WithConfiguration)}"/> method 
        /// results in a <see cref="ArgumentException"/> being thrown.
        /// </summary>
        [Test]
        public void Build_WithoutConfiguration_ThrowsArgumentException()
        {
            var builder = new AnimalsBuilder();

            try
            {
                builder.Build();
                Assert.Fail($"{nameof(ArgumentException)} not thrown.");
            }
            catch (ArgumentException exception)
            {
                Assert.AreEqual(exception.Message, $"{nameof(AnimalsBuilder.WithConfiguration)} not called.");
            }
        }

        /// <summary>
        /// Ensure that providing a <see cref="ZooConfiguration"/> sets the correct number of <see cref="IZooAnimal"/> entities.
        /// </summary>
        [Test]
        public void Builder_GivenConfiguration_ReturnsNumberOfAnimals()
        {
            const int elephants = 25;
            const int giraffes = 32;
            const int monkeys = 22;

            var builderConfiguration = new ZooConfiguration();
            builderConfiguration.ElephantCount = elephants;
            builderConfiguration.GiraffeCount = giraffes;
            builderConfiguration.MonkeysCount = monkeys;


            var builder = new AnimalsBuilder();
            var animals = builder
                .WithConfiguration(builderConfiguration)
                .Build();

            Assert.IsTrue(animals.OfType<Elephant>().Count() == elephants);
            Assert.IsTrue(animals.OfType<Giraffe>().Count() == giraffes);
            Assert.IsTrue(animals.OfType<Monkey>().Count() == monkeys);
        }
    }
}
