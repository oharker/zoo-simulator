﻿using NUnit.Framework;
using ZooSimulator.Entities.Enums;
using ZooSimulator.Services.Extensions;

namespace ZooSimulator.Services.Tests.Extensions
{
    /// <summary>
    /// Tests for the <see cref="ZooAnimalStatusExtensions"/> class.
    /// </summary>
    public class ZooAnimalStatusExtensionsTests
    {
        /// <summary>
        /// Ensure that the correct string is returned for each state.
        /// </summary>
        /// <param name="status">The status of the view model.</param>
        /// <returns>The string of the state.</returns>
        [TestCase(ZooAnimalStatus.Deceased, ExpectedResult = "Deceased")]
        [TestCase(ZooAnimalStatus.WalkingImpaired, ExpectedResult = "Cannot Walk")]
        [TestCase(ZooAnimalStatus.Alive, ExpectedResult = "Alive")]
        public string ToViewModelString_GivenEnum_ReturnsString(ZooAnimalStatus status)
        {
            return status.ToViewModelString();
        }

    }
}
