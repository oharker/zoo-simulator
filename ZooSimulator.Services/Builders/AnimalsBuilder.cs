﻿using System;
using System.Collections.Generic;
using ZooSimulator.Entities.Animals;
using ZooSimulator.Entities.Animals.Interfaces;

namespace ZooSimulator.Services.Configuration
{
    /// <summary>
    /// A builder for constructing <see cref="IZooAnimal"/> entities based on the configuration provided.
    /// </summary>
    public class AnimalsBuilder
    {
        /// <summary>
        /// Member store for the configuration.
        /// </summary>
        private ZooConfiguration _configuration;

        /// <summary>
        /// Add the provided <see cref="ZooConfiguration"/> to the creation path of the objects.
        /// </summary>
        /// <param name="configuration">The configuration of the animals to be built.</param>
        /// <returns>This builder.</returns>
        public AnimalsBuilder WithConfiguration(ZooConfiguration configuration)
        {
            _configuration = configuration;
            return this;
        }

        /// <summary>
        /// Build the 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IZooAnimal> Build()
        {
            Validate();
            var animals = new List<IZooAnimal>();

            // Create the number of animals via the configuration provided.
            var giraffes = CreateAnimalsOfType<Giraffe>(_configuration.GiraffeCount);
            var elephants = CreateAnimalsOfType<Elephant>(_configuration.ElephantCount);
            var monkeys = CreateAnimalsOfType<Monkey>(_configuration.MonkeysCount);

            animals.AddRange(giraffes);
            animals.AddRange(elephants);
            animals.AddRange(monkeys);

            return animals;
        }

        /// <summary>
        /// Create animals of the provided type and number.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="IZooAnimal"/> to create.</typeparam>
        /// <param name="number">The number of animals to create.</param>
        /// <returns>A collection of animals of the provided type and number.</returns>
        private IEnumerable<IZooAnimal> CreateAnimalsOfType<T>(int number) 
            where T : IZooAnimal, new()
        {
            var animals = new List<IZooAnimal>();
            for (int i = 0; i < number; i++)
            {
                animals.Add(new T());
            }

            return animals;
        }

        /// <summary>
        /// Validate that the configuration has been set.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if the <see cref="_configuration"/> has not been set.</exception>
        private void Validate()
        {
            if (_configuration == null)
            {
                throw new ArgumentException($"{nameof(WithConfiguration)} not called.");
            }
        }
    }
}
