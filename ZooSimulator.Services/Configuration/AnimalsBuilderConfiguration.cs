﻿namespace ZooSimulator.Services.Configuration
{
    public class ZooConfiguration
    {
        /// <summary>
        /// Gets or sets the number of giraffes.
        /// </summary>
        public int GiraffeCount { get; set;}

        /// <summary>
        /// Gets or sets the number of elephants.
        /// </summary>
        public int ElephantCount { get; set; }

        /// <summary>
        /// Gets or sets the number of monkeys.
        /// </summary>
        public int MonkeysCount { get; set; }
    }
}
