﻿namespace ZooSimulator.Services.Configuration
{
    /// <summary>
    /// Configuration for the zoo time service.
    /// </summary>
    public class ZooTimeServiceConfiguration
    {
        /// <summary>
        /// The number of real-time seconds an hour will comprise of.
        /// </summary>
        public int SecondsToHours { get; set; }
    }
}
