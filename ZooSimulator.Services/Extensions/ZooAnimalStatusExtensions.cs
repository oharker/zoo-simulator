﻿using ZooSimulator.Entities.Enums;

namespace ZooSimulator.Services.Extensions
{
    /// <summary>
    /// Extension methods for the <see cref="ZooAnimalStatus"/> enum.
    /// </summary>
    public static class ZooAnimalStatusExtensions
    {
        /// <summary>
        /// Convert the provided <see cref="ZooAnimalStatus"/> to a string value.
        /// </summary>
        /// <param name="status">The status to convert to a string value.</param>
        /// <returns>The string equivalent of the provided <see cref="ZooAnimalStatus"/>.</returns>
        public static string ToViewModelString(this ZooAnimalStatus status)
        {
            var returnValue = "Unknown";
            switch (status)
            {
                case ZooAnimalStatus.Alive:
                    returnValue = "Alive";
                    break;
                case ZooAnimalStatus.WalkingImpaired:
                    returnValue = "Cannot Walk";
                    break;
                case ZooAnimalStatus.Deceased:
                    returnValue = "Deceased";
                    break;
            }

            return returnValue;
        }
    }
}
