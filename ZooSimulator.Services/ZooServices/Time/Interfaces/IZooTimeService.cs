﻿using System;

namespace ZooSimulator.Services.ZooServices.Interfaces.Time.Interfaces
{
    /// <summary>
    /// Service for managing time within the zoo.
    /// </summary>
    public interface IZooTimeService
    {
        /// <summary>
        /// Retrieve the time for the zoo.
        /// </summary>
        /// <returns>The time for the zoo.</returns>
        DateTime GetTime();

        /// <summary>
        /// Event raised when the hour has been changed
        /// </summary>
        event Action OnHourChange;

        /// <summary>
        /// Event raised when the time has been changed.
        /// </summary>
        event Action<DateTime> OnTimeChange;

        /// <summary>
        /// Start the time service.
        /// </summary>
        void Start();
    }
}