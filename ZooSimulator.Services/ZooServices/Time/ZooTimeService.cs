﻿using Microsoft.Extensions.Options;
using System;
using System.Timers;
using ZooSimulator.Services.Configuration;
using ZooSimulator.Services.ZooServices.Interfaces.Time.Interfaces;

namespace ZooSimulator.Services.ZooServices.Time
{
    /// <summary>
    /// Manages the simulation of time within the zoo.
    /// </summary>
    public class ZooTimeService : IZooTimeService
    {
        /// <summary>
        /// The underlying time for the zoo.
        /// </summary>
        private static DateTime underlyingTime = default(DateTime);

        /// <summary>
        /// The timer for the zoo.
        /// </summary>
        private static Timer _timer;

        /// <summary>
        /// Event fired when an hour has changed within the service.
        /// </summary>
        public event Action OnHourChange;

        /// <summary>
        /// Event fired when time has changed within the service.
        /// </summary>
        public event Action<DateTime> OnTimeChange;

        /// <summary>
        /// The configuration for the zoo service.
        /// </summary>
        public ZooTimeServiceConfiguration _config;

        /// <summary>
        /// The service for the management of time within the zoo.
        /// </summary>
        /// <param name="configuration">The configuration of the zoo time service.</param>
        public ZooTimeService(IOptions<ZooTimeServiceConfiguration> configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Configuration cannot be null.");
            }

            _config = configuration.Value;
        }

        /// <summary>
        /// Start the time service.
        /// </summary>
        public void Start()
        {
            _timer = new Timer()
            {
                Interval = 1000
            };

            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
        }

        /// <summary>
        /// Event fired when the timer has elapsed.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The arguments of the event.</param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            AddMinutes();
        }

        /// <summary>
        /// Add an hour to the zoo time.
        /// </summary>
        public void AddMinutes()
        {
            // Calculate the number of minutes to increase a second.
            var minutesToAdd = 60 / _config.SecondsToHours;

            // Determine if the hour has changed.
            var currentHour = underlyingTime.Hour;
            underlyingTime = underlyingTime.AddMinutes(minutesToAdd);

            // Fire the on time change event.
            OnTimeChange?.Invoke(underlyingTime);

            // If the hour has changed, fire the on hour change event.
            if (currentHour != underlyingTime.Hour)
            {
                OnHourChange?.Invoke();
            }
        }

        /// <summary>
        /// Retrieve the time for the zoo.
        /// </summary>
        /// <returns>The time for the zoo.</returns>
        public DateTime GetTime()
        {
            return underlyingTime;
        }
    }
}
