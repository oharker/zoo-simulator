﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZooSimulator.Entities.Animals.Interfaces;
using ZooSimulator.Entities.Food;

namespace ZooSimulator.Services.ZooServices.Zoo
{
    /// <summary>
    /// Service responsible for feeding <see cref="IZooAnimal"/> entities.
    /// </summary>
    public class FeederService
    {
        /// <summary>
        /// The map of type of animals to food.
        /// </summary>
        private Dictionary<Type, IFood> foodMap = new Dictionary<Type, IFood>();

        /// <summary>
        /// Feed the entire zoo.
        /// </summary>
        /// <param name="zoo">The zoo to feed.</param>
        public void FeedZoo(Entities.Zoo.Zoo zoo)
        {
            // Feed every animal, as long as they are not deceased.
            foreach(var animal in zoo.Animals.Where(animal => animal.Status != Entities.Enums.ZooAnimalStatus.Deceased))
            {
                FeedAnimal(animal);
            }
        }

        /// <summary>
        /// Feed the provided zoo animal.
        /// </summary>
        /// <param name="zooAnimal">The Zoo animal to feed.</param>
        private void FeedAnimal(IZooAnimal zooAnimal)
        {
            var type = zooAnimal.GetType();
            var food = GetFoodForType(type);

            zooAnimal.Eat(food);
        }

        /// <summary>
        /// Create the food for the provided type.
        /// </summary>
        /// <param name="type">The type to create food for.</param>
        /// <returns>The food for the provided type.</returns>
        private IFood GetFoodForType(Type type)
        {
            IFood food = null;
            if (foodMap.ContainsKey(type))
            {
                food = foodMap[type];
            }
            else
            {
                food = new Food();
                foodMap.Add(type, food); 
            }

            return food;
        }
    }
}
