﻿
using System;

namespace ZooSimulator.Services.ZooServices.Zoo.Interfaces
{
    /// <summary>
    /// Interface for the service of interacting with a <see cref="Zoo"/> entity.
    /// </summary>
    public interface IZooService
    {
        /// <summary>
        /// Feed the animals within the zoo.
        /// </summary>
        void FeedAnimals();

        /// <summary>
        /// Event fired when the zoo has changed state within the service.
        /// </summary>
        event Action OnZooStateChange;

        /// <summary>
        /// Retrieve the zoo the service is responsible for.
        /// </summary>
        /// <returns>The <see cref="Zoo"/> the service is responsible for.</returns>
        Entities.Zoo.Zoo GetZoo();

        /// <summary>
        /// Deteriate the condition of the animals within the zoo.
        /// </summary>
        void DeteriateAnimals();
    }
}
