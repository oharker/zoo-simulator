﻿using Microsoft.Extensions.Options;
using System;
using System.Linq;
using ZooSimulator.Services.Configuration;
using ZooSimulator.Services.ZooServices.Zoo;
using ZooSimulator.Services.ZooServices.Zoo.Interfaces;

namespace ZooSimulator.Services.ZooServices
{
    /// <summary>
    /// Service for interacting with <see cref="Zoo"/> entities.
    /// </summary>
    public class ZooService : IZooService
    {
        /// <summary>
        /// Event fired when the zoo has changed state within the service.
        /// </summary>
        public event Action OnZooStateChange;

        /// <summary>
        /// The zoo entity the service is responsible for.
        /// </summary>
        private Entities.Zoo.Zoo _zoo;

        /// <summary>
        /// Initializes a new insance of the <see cref="ZooService"/>.
        /// </summary>
        /// <param name="zoo">Zoo the service is responsible for.</param>
        /// <param name="configuration">The configuration for the zoo to be created.</param>
        public ZooService(IOptions<ZooConfiguration> configuration)
        {
            var builder = new AnimalsBuilder();
            var animals = builder
                .WithConfiguration(configuration?.Value)
                .Build();

            _zoo = new Entities.Zoo.Zoo(animals);
        }

        /// <summary>
        /// Feed the animals within the zoo.
        /// </summary>
        public void FeedAnimals()
        {
            var feederService = new FeederService();
            feederService.FeedZoo(_zoo);

            OnZooStateChange?.Invoke();
        }

        /// <summary>
        /// Retrieve the zoo that the service is responsible for.
        /// </summary>
        /// <returns>The zoo that the service is responsible for.</returns>
        public ZooSimulator.Entities.Zoo.Zoo GetZoo()
        {
            return _zoo;
        }

        /// <summary>
        /// Deteriate the animals within the zoo.
        /// </summary>
        public void DeteriateAnimals()
        {
            var random = new Random();
            foreach(var animal in _zoo.Animals.Where(m => m.Status != Entities.Enums.ZooAnimalStatus.Deceased))
            {
                animal.DeteriorateHealth(random.Next(0, 20));
                if (animal.Health.IsBelowMinimumRequirements())
                {
                    animal.MarkAsDeceased();
                }
            }

            OnZooStateChange?.Invoke();
        }
    }
}
